from scipy.integrate import solve_ivp
from scipy.integrate import RK45
import numpy as np
import matplotlib.pyplot as plt
from functions import *

# result = RK45()
result = solve_ivp(f2, [1, 100], [0.1, 0], method='RK45', max_step=0.1)

# print(result.y)

plt.plot(result.t, result.y[0], label='First Line')
# plt.plot(result.t, result.y[1], label='Second Line')

plt.xlabel('t')
plt.ylabel('y')
plt.title('Interesting Graph\nCheck it out')
plt.legend()
plt.show()