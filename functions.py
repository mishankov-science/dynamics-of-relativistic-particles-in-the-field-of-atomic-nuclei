# from math import sqrt, exp
from numpy import sqrt, exp

def f1(x, y): return y


U0 = 0.1
a = 0.0205
b = -0.07
d = 0.1


# def f2(x, y): 
#     return -x - b * exp(-d * (x * exp(-1. / x))) * sqrt(
#     1 + (d / a) * (y ** 2 + x ** 2) * exp(2 * d * x * exp(-1. / x))) + a * (1 + 1. / x) * exp(
#     -1. / x - 2 * d * x * exp(-1. / x))

def f2(x, y): 
    return -x - b * exp(-d * (x * exp(-1. / x))) * sqrt(
    1 + (d / a) * (y ** 2 + x ** 2) * exp(2 * d * x * exp(-1. / x))) + a * (1 + 1. / x) * exp(
    -1. / x - 2 * d * x * exp(-1. / x))

# test
def exponential_decay(t, y): 
    print('t={} {}, y={} {}'.format(t, type(t), y, type(y)))
    print('-0.5 * y={}'.format(-0.5 * y))
    return -0.5 * y